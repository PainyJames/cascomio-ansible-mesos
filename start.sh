#!/bin/bash

echo "Cleaning unused docker images/containers"

docker system prune -f

echo "Removing previously created images/containers"

docker-compose down

docker rmi cascomio-ssh

docker rmi cascomio-ansible

echo "Creating images"

cd ssh

docker build -t cascomio-ssh .

cd ../ansible

docker build -t cascomio-ansible .

cd ..

echo "Creating containers"

docker-compose up -d -t 10000000000
