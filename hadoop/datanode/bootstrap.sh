#!/bin/bash

NAMENODE=$(ip route | \
        grep "eth0  src" | \
        cut -f6 -d' ' | \
        xargs -i echo {}:8123/v1/services/_hadoop-namenode._tcp.marathon.mesos | \
        xargs -i curl -ss {} | \
        grep -E 'ip|port' | \
        grep -oe '\(\([0-9]\)\{4\}\|\(\([0-9]\{1,3\}\.*\)\{4\}\)\)' -m 2 | \
	awk '{addr=$0; getline; print  addr ":" $0;}')

echo "Namenode found on " $NAMENODE

sed -i "s/NAMENODE/$NAMENODE/" /etc/hadoop/core-site.xml

/usr/sbin/sshd -E /var/log/authlog -D &

hadoop-daemon.sh --config $HADOOP_CONF_DIR start datanode &

tail -f /dev/null