#!/bin/bash

docker rmi hadoop

docker rm hadoop-namenode
docker rmi hadoop-namenode

docker rm hadoop-datanode
docker rmi hadoop-datanode