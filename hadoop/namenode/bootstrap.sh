#!/bin/bash

hdfs namenode -format

/usr/sbin/sshd -E /var/log/authlog -D &

hadoop-daemon.sh --config $HADOOP_CONF_DIR start namenode &

tail -f /dev/null