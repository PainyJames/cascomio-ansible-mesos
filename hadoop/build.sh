#!/bin/bash

cd base

docker build -t hadoop .

cd ../namenode

docker build -t hadoop-namenode .

cd ../datanode

docker build -t hadoop-datanode .

cd ..