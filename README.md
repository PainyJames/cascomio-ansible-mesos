# Mesos cluster automatized with docker-compose and ansible

Just a small repo that sets up a small mesos cluster.
Docker compose sets up 4 containers, 1 as a master node and 3 as slave nodes. There is another node that runs the ansible tasks.

### How to run it

There is an ` start.sh  ` script that you can run which creates both the **ansible** and **ssh** images.

Once those are created, you could do the same as with any other docker-compose project. It may timeout if the network is not fantastic so it's possible that you need to run something like the following.

```sh
$ docker-compose up -d -t 10000000000000
```